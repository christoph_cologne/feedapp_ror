# Ruby on Rails Application #

Uses Mongoid as ODM to connect to MongoDB. jBuilder is used create a CRUD and display JSON

### How to use ###

* Run 'rails server' to start Webrick Server
* Go to 'localhost/products' to see crud and products.json to see dynamically created JSON