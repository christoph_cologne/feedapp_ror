json.array!(@Product) do |product|
  # json.extract! product, :name, :price, :img
  json.name product.name
  json.price product.price.to_s.to_f
  json.img product.img
  #json.url product_url(product, format: :json)
end
