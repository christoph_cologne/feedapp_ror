json.array!(@products) do |product|
  json.extract! product, :id, :name, :price, :img
  json.url product_url(product, format: :json)
end
